import numpy as np
import matplotlib.pyplot as plt

class mascara():
	
	def __init__(self, maxSpan, maxN, dots):
		'''
		maxSpan: must be the difference between the minimum frequency and the maximum frequency.
		dots:	this is a matrix [n,2], each tuple corresponds to a discontinuous point of a mask
				first column corresponds to frecuencies and second column corresponds to power values.
 		'''
		self.maxSpan = maxSpan
		self.maxN = maxN
		self.dots = dots
		self.step = self.maxSpan/float(self.maxN) #this was df
		
	def gen_seg(self, ini, fin):
		'''
		Generates a segment between two tuples.
		This function takes two tuples and creates a segment.
		
		'''	
		#print ini,fin #instrumentation line
		size = 	((fin[0] - ini[0]) / float(self.step))
		#print size
		if (size - int(size))>0.5: 
			 size = np.ceil(size)
		else:
			size = np.floor(size)
		#print 'size',size #instrumentation line
		slope = ((fin[1]-ini[1])/float(fin[0]-ini[0]))
		#print 'slope', slope   #instrumentation line
		smask = np.zeros([int(size),2])
		for i in xrange(int(size)):
			smask[i,0] = i*self.step + ini[0]
			smask[i,1] = i*slope*self.step + ini[1]
		return smask
	
	def gen_mask(self, dots):
		'''
		This function generates a mask by concatenating segments
		'''
		mask = []
		for s in xrange(len(dots)-1):
			mask.append(self.gen_seg(dots[s,:], dots[s+1,:]))
		mask = np.asarray(mask)
		mask = np.vstack(mask)
		return	mask
		
if __name__ == "__main__":
	#maxN = 1024
	#dots = np.array([(100,30),(400,20),(500,10),(850,20),(2000,40)])
	#maxSpan = dots[len(dots)-1,0]-dots[0,0] #max freq - min freq
	#print maxSpan
	#m = mascara(maxSpan, maxN, dots);
	#print dots
	#mask = m.gen_mask(dots)
	#print mask
	#plt.plot(mask[:,0],mask[:,1])
	#print len(mask)	
	#plt.show()
		
