import numpy

class mask():

    def __init__(self, x, y, p):
        """Constructor"""

        self.x = x
        self.y = y
        self.p = p

    def get_mask(self):

        axis = numpy.linspace(self.x[0], self.x[-1], self.p)
        mask = []
        for i in xrange(len(self.x)-1):
            if i < len(self.x) - 2:
                filtered_axis = axis[(axis < self.x[i+1]) & (axis >= self.x[i])]
                numel = filtered_axis.shape[0]
                mask += (numpy.linspace(self.y[i], self.y[i+1], numel, False)).tolist()
            else:
                filtered_axis = axis[(axis <= self.x[i+1]) & (axis >= self.x[i])]
                numel = filtered_axis.shape[0]
                mask += (numpy.linspace(self.y[i], self.y[i+1], numel, True)).tolist()

        return mask
